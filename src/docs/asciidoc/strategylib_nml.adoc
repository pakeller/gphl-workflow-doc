[[strategylibnml,the `strategylib.nml` file]]
=== The `strategylib.nml` file

NOTE: The choice of Fortran 95 namelist format for this file has turned out to
be problematic. In the future, this file will use a different format.

This file contains so-called "standalone" strategies, namely data collection
strategies that are known _a priori_ and don't depend on any properties of the
sample. Currently, two types of strategy are contained within this file:

* one or more strategies to be used for collecting an initial small number of
images for the purposes of indexing, and deriving the orientation matrix of the
mounted sample.

* one or more strategies for the diffractometer calibration workflow.

Note that a strategy in this context only defines the "geometric" part of a data
collection, i.e. which part of reciprocal space is going to be collected
expressed in terms of the instrumentation. Other information such as exposure
time, image width and interleaving (if any) is defined during the workflow
execution.

The conceptual structure of a strategy is essentially the same as that defined
in the Abstract Beamline Interface, see {abiurl} and {abiurl}/wiki. A "Sweep" is
defined by the aggregation of a set of instrumental settings, and assigned a
starting setting of the scan axis and the length of the sweep. The strategy
itself is defined as a collection of sweeps. This is illustrated in
<<strategydef>>.


.The structure of a strategy definition
[[strategydef]]
image::strategy.svg[scaledwidth=80%,width=80%,role="text-center"]

The three namelist groups holding instrumental settings all have an `id`
parameter. This parameter holds a value which is used to refer back to the
setting in sweep definitions later in the file. The value is a string identifier
that is unique within the file. If the format of the value conforms to
https://en.wikipedia.org/wiki/Universally_unique_identifier#Format[the text
representation of a uuid], the value is used as-is. Otherwise it is converted
internally to a uuid when the file is parsed.

WARNING: an `id` must have been assigned in this file before it is referenced.
Forward references to `id` values don't work.

The following subsections list the namelist groups and parameters required in
this file.

==== The `sdcp_goniostat_setting_list` group

This group is used to define a setting for the sample goniostat that may be used
in one or more sweep definitions later in the file.

`id`:: A string that is used to uniquely refer to this goniostat setting in
sweep definitions.

[[scan_axis_name,the `scan_axis_name` parameter]]
`scan_axis_name`:: The name of the axis that is going to be used as the rotation
axis for data collection. The value of this parameter must match one of the
names that have been assigned to xref:gonio_axis_names[].

`goniostat_axis_settings`:: A list of settings for the goniostat rotation axes
that are going to be used for this sweep. The order and number of the values
must match the names that have been assigned to xref:gonio_axis_names[]. The
setting provided for the rotation axis is in effect a dummy value and will not
be used.

[[sdcp_detector_setting_list,the `sdcp_detector_setting_list` group]]
==== The `sdcp_detector_setting_list` group

This group is used to define a setting for the detector that may be used
in one or more sweep definitions later in the file.

For a detector whose position is fixed (in the sense that its position will not
be changed in normal user-mode operation) this group can be omitted, together
with the variable `detector_setting_id` from `stratcal_sweep_list`. The detector
position will be taken from <<det_org_dist>> or
<<det_gonio_axis_datum_settings>> of the current diffractometer calibration.

`id`:: A string that is used to uniquely refer to this detector setting in sweep
definitions.

`detector_axis_settings`:: A list of settings for the detector positioning axes
that are going to be used for this sweep. The order and number of the values
must match the names that have been assigned to xref:det_gonio_axis_names[].

NOTE: In a future version, a facility will be provided to change the values
specified here prior to collecting data.

==== The `beam_setting_list` group

This group is used to define the wavelength used in the strategy.

`id`:: A string that is used to uniquely refer to this beam setting in sweep
definitions.

`lambda`:: The wavelength in Ångström units

NOTE: In a future version, a facility will be provided to change the value
specified here prior to collecting data.

==== The `stratcal_sweep_list` group

This group is used to define a geometric Sweep, according to the
{abiurl}/wiki/Terminology#sweep[definition] provided in the Abstract Beamline
Interface.

`goniostat_setting_id`:: The value of the `id` parameter of the
`sdcp_goniostat_setting_list` group containing the goniostat settings for this
sweep.

`detector_setting_id`:: The value of the `id` parameter of the
`sdcp_detector_setting_list` group containing the detector positioning settings
for this sweep.

`beam_setting_id`:: The value of the `id` parameter of the `beam_setting_list`
group containing the wavelength for this sweep.

`start_deg`:: The starting setting of the rotation axis specified by the
relevant xref:scan_axis_name[`scan_axis_name` parameter].

`length_deg`:: The "length" of the sweep in degrees, i.e. the offset from the
value assigned to `start_deg` that is the end of the sweep.

`repeat`, `offset` (optional):: If specified, these two items will cause
`repeat{nbsp}-{nbsp}1` additional copies of the sweep to be generated when processing
the file. Each sweep in the resulting strategy will have a start setting of
`start_deg{nbsp}+{nbsp}(_n_{nbsp}-{nbsp}1)*offset`, where `_n_` is the 1-based
number of the sweep (i.e. 1 for the original sweep, 2 for the first copy, etc.).
These two parameters must either be both unassigned, or both have values assigned,
and the assigned values must both be positive.

==== The `sdcp_strategy_list` group

This group marks the start of the definition of the strategy. All subsequent
instances of `stratcal_sweep_list` up to the next instance of
`sdcp_strategy_list` (or the end of the file) make up the strategy.

Where the `stratcal_sweep_list{nbsp}repeat` and `stratcal_sweep_list{nbsp}offset`
parameters have been assigned, the strategy must only contain a single
`stratcal_sweep_list`. When the workflow communicates the strategy to the
BCS, the strategy will contain the original sweep defined in this file, and
all generated copies of the sweep as described above. The fields
`Strategy::sweepRepeat` and `Strategy::sweepOffset` will also be populated
(see <<strategydef>>).

WARNING: There is no guarantee that the {abiurl}/wiki/Terminology#scan[scans]
that make up the resulting data collection will be collected in an order that
reflects the order of the sweeps defined in this file. In a future version some
more control over the ordering will be implemented.

`strategy_name`:: The name to identify the strategy. Where the file contains
more than one strategy, each one must have a name that is unique within the
file.

`type`:: Currently this must have the value `'geometric'`. In the future, other
strategy types will be possible.

`allowed_widths` (optional):: A list of allowed image widths for this strategy.
The widths should be chosen such that they all are integer divisors into both
the sweeps that make up this strategy and any gaps between sweeps that share a
common orientation and wavelength.

`default_width_idx` (optional):: Where a list of allowed widths is specified,
this parameter gives the zero-based index of the width that should be the
default.
