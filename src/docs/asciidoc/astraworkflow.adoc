= Configuring and using the ASTRA workflows
:doctype: book
:author: Peter Keller
:email: pkeller@globalphasing.com
:revdate: 2018-08-17
:sectnumlevels: 4
// Hmm, this could be more useful.
// see https://github.com/asciidoctor/asciidoctor/issues/2365
:xrefstyle: short
// highlight.js always pulls in a script from an external URL.
// We use CodeRay for both html5 and PDF
:source-highlighter: coderay
:toc:
:toclevels: 3
[colophon]
(C) 2018, 2022 Global Phasing Ltd.

ifeval::["{backend}" == "html5"]
// This produces warnings during document generation using asciidoctor at the command-line,
// but the build with the Gradle asciidoctor plugin is fine with this
:imagesdir: images
endif::[]

ifeval::["{backend}" == "pdf"]
// {wj} inserts a space in the PDF backend, so we are better off without it.
// Set it to the null string.
:wj:
:imagesdir: src/resources/images
endif::[]

:sectnums:

:abiurl: https://github.com/githubgphl/gphl-abstract-beamline
:xdsurl: http://xds.mpimf-heidelberg.mpg.de
:xdsdocurl: {xdsurl}/html_doc
:xdsparamsurl: {xdsdocurl}/xds_parameters.html#
:jdocsurl: https://docs.oracle.com/javase/8/docs

== Overview

The BCS controls the workflow startup and behaviour with a combination of
command-line options and Java properties. Some of these options and properties
are specific to the particular workflow being run, others are common to all
workflows. Once started, the workflow communicates with the BCS, sending it
requests for data or requests to perform certain operations (such as collecting
images). In between interactions with the BCS, the workflow runs other programs
("workflow-side applications") whose output determines the course of the
 workflow.

The data that are output by workflow-side applications and that are sent by the
BCS to the workflow are gathered by the workflow into the "persistence layer".
The persistence layer serves as a canonical resource for data accumulated during
a workflow enactment, and allows the workflow to access persisted data in an
application-independent way. It also ensures that every request sent from the
workflow to the BCS contains all required data, allowing the interaction between
the workflow and the BCS to be stateless.

== Beamline configuration

The beamline configuration that the workflow requires is contained in a single
directory. The workflow uses files with specific names in this directory to
initialise the persistence layer (other files are ignored).

The format of most of these files is a subset of Fortran 90 namelist input
format. Note that the names of variables and namelist groups is
case-independent, but in general string data should be assumed to be
case-dependent.


:sectnums!:
=== Some notes about Fortran 95 namelist format

Much of the beamline configuration is expressed in a subset of Fortran 95
namelist format, which is easy to understand for the most part. There are some
subtleties though which are mentioned here.

An unquoted `!` introduces a comment. The comment extends to the end of the
line.

In a list of values, the separator between non-empty values is either
whitespace, a `,` character, or any combination of whitespace and a `,`
character. A value may be repeated by prepending a positive integer count value
followed immediately by a `*` character.

Whitespace (including newlines) outside a quoted string is generally not
significant, and can be inserted freely to help readability. There are two
exceptions to this:

* at the start of a namelist group there must not be any whitespace between the `&`
character and the group name.

// Using `'*'` for both *'s results in them being interpreted as a pair of
// formatting characters. Using the `+...+` form for the first one avoids
// this, see
// http://asciidoctor.org/docs/user-manual/#escaping-unconstrained-quotes
* when using the `r*` form to specify a count for a repeated value.
Whitespace between `r` and the `+*+` character is a syntax error. Whitespace
following the `*` character means a sequence of _r_ null values.

A single null value is specified by a sequence of two separator characters,
optionally surrounded and/or separated by whitespace. The separator characters
are:

* The `=` after a variable name
* The `/` that ends a namelist group
* A `,`

A sequence of null values can be specified using the form `r*` followed by
whitespace and/or a `,` or the namelist group's terminating `/`.

As an example, in this namelist format data:

[listing]
&example
var1= , 2 3, , 4, ,
var2= 1 2 3, , 4,
var3= 1 2*2 2* 3
var4= 1 2 3, , 4,
/

the variables are assigned to as follows:

[listing]

var1: [null, 2, 3, null, 4, null]
var2: [1, 2, 3, null, 4]
var3: [1, 2, 2, null, null, 3]
var4: [1, 2, 3, null, 4, null]

When adding a new assignment to the end of a namelist group, or moving an
assignment to or from the end of a namelist group, it is easy to add or remove a
trailing null without realising it.

A variable with no values following the `=` sign is a syntax error. To assign
nothing to a variable, simply leave it out of the namelist group.

A namelist group with no variable assignments is legal, e.g.:

 &example
 /

String values should be delimited with either single quotes or double quotes. To
include the delimiter as a character in the string, repeat it. For example, the
following two values both contain the string `it's`:

  'it''s'
  "it's"

For booleans, Fortran-style logical literals should be used as follows:

true:: an unquoted value starting with `t`, `T`, `.t` or `.T`
false:: an unquoted value starting with `f`, `F`, `.f` or `.F`


NOTE: The strictly-conformant forms of Fortran logical literals are `.TRUE.` and
`.FALSE.`. Different Fortran compilers have varying degrees of support for other
forms, such as lower-case, omission of the periods and the use of the output
forms `T` and `F` as input. This would only be an issue if the namelist data
described in this document were to be read by native Fortran applications as
well as by the workflow software itself. This is not currently the case, so the
more relaxed logical literals described here may be used freely.

NOTE: For backwards compatibility with older versions of the workflow software,
the quoted string values `'True'` and `'False'` may also be used, however this
form is now deprecated.


==== Restrictions specific to this implementation

A variable name must not be qualified with a subscript, stride or substring
range expression.

String values cannot span lines.

The `/` character that ends a namelist group must be on a line by itself.

Old-style namelist input (which uses `$`, and `&END` or `$END` to delimit
a namelist group) is not supported.

The use of a `;` character as a value separator is not supported.

NOTE: A good guide to the complete namelist format can be found in the
https://www.ibm.com/support/knowledgecenter/en/SSAT4T[IBM XL Fortran] language
reference. Find the "Namelist input" section. At the time of writing, this is at
https://www.ibm.com/support/knowledgecenter/en/SSAT4T_15.1.6/com.ibm.xlf1516.lelinux.doc/language_ref/namelistinput.html


:sectnums:

// TODO: work out how to set includedir appropriately in the Eclipse/Asciidoctor context
include::{includedir}/instrumentation_nml.adoc[]

include::{includedir}/strategylib_nml.adoc[]

include::{includedir}/filenametemplates_xml.adoc[]

include::{includedir}/transcal_dat.adoc[]

[[idxref.inp]]
=== The `IDXREF.INP` file

The MX Experimental Workflow performs a preliminary characterisation data collection, which is indexed by {xdsdocurl}/xds_program.html#IDXREF[IDXREF] from the XDS package to provide the orientation matrix of the sample.
The workflow uses information about the beamline and data collection to generate the {xdsdocurl}/xds_files.html#XDS.INP[`XDS.INP`] file that is input to XDS, however additional beamline-specific assignments to XDS parameters may be needed.
These additional assignments should be defined in a file in the beamline configuration directory called `IDXREF.INP`.

The same parameter can be assigned to multiple times, e.g. {xdsdocurl}/xds_parameters.html#EXCLUDE_RESOLUTION_RANGE=[EXCLUDE_RESOLUTION_RANGE].
The assignments in `IDXREF.INP` are included in the generated `XDS.INP` file that is input to the indexing process, overriding any and all other assignments to the same parameters if present.

`IDXREF.INP` should be in the same format as `XDS.INP`, except that having more than one assignment per line is not allowed.
Comments and blank lines are allowed.

NOTE: It is not necessary to provide an `IDXREF.INP` file if no additional XDS-specific parameters are requried.
The XDS parameters {xdsdocurl}/xds_parameters.html#DETECTOR=[DETECTOR] and {xdsdocurl}/xds_parameters.html#OVERLOAD=[OVERLOAD] are mandatory for the indexing step, and are specified with the corresponding `XDS_...` parameters in <<instrumentation.nml>>. 
The XDS parameter {xdsdocurl}/xds_parameters.html#SENSOR_THICKNESS=[SENSOR_THICKNESS] is specified in <<instrumentation.nml>> with the <<d_sensor>> parameter rather than in `IDXREF.INP`

NOTE: In the future, the workflow will perform the initial indexing using an
https://www.globalphasing.com/autoproc/[autoPROC]-provided mechanism rather than
by invoking XDS directly. At that point, the use of `IDXREF.INP` will be
deprecated, and beamline-specific configuration will be done through autoPROC
instead.

include::{includedir}/invocation.adoc[]


== Glossary and Abbreviations

NOTE: Some scientific terminology used by the workflow is discussed in the
Abstract Beamline Interface wiki at {abiurl}

BCS: Beamline Control Software
